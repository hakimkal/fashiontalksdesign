jQuery(document).ready(function($) {

    var owl = $(".owl-carousel");
        owl.owlCarousel({
            items : 1, 
            loop : true,
            autoplay:true,
            autoplayTimeout: 5000,
            itemsDesktop : [992,1],
            itemsDesktopSmall : [768,1], 
            itemsTablet: [480,1], 
            itemsMobile : [320,1]
        });

});